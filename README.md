# OpenML dataset: Online-Food-Delivery-Preferences-Bangalore-region

https://www.openml.org/d/43446

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context of dataset
There has been a rise in the demand of online delivery in the metropolitan cities such as Bangalore in India. The question about why this increase in the demand has always been a lingering question. So a survey is conducted and the data is presented.
Content
The dataset has nearly 55 variables based on the following titles

Demographics of consumers
Overall/general purchase decision 
Time of delivery influencing the purchase decision 
Rating of Restaurant influencing the purchase decision 

This dataset can be useful for

Classification modelling (Whether this consumer will buy again or not)
Text analysis (Reviews of consumers)
Geo-spatial Analysis (location-latitude and longitude of consumers)

Inspiration
This dataset was collected as a part of my masters thesis

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43446) of an [OpenML dataset](https://www.openml.org/d/43446). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43446/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43446/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43446/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

